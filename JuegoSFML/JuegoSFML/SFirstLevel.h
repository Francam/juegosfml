#pragma once
#include "Scene.h"
#include "Reg.h"
#include <SFML\Graphics.hpp>
#include "Player.h"
#include "Background.h"
#include "Gate.h"
#include "EnemigoFollow.h"

class SFirstLevel :
	public Scene
{
private:
	Scene* parentScene;
	Player* py;
	EnemigoFollow* mainEnemy;
	Background* bk;
	Gate* gate;
	sf::Text* text_TimeText;
	sf::Font fuenteTexto;
	float timer;
	bool gameOver;
	bool endLevel;
public:
	SFirstLevel(sf::RenderWindow* window, Scene* _parentScene);
	~SFirstLevel();
	virtual void Update(sf::Time dt);
	void MovementPlayer(Player* p);
	void Collisiones(Player* p, Objects* en);
};