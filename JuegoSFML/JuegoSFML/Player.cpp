#include "Player.h"
#include "Reg.h"


void Player::Update(sf::Time dt)
{
	if (playerAlive)
	{
		sprite.move(leftright*dt.asSeconds(), updown*dt.asSeconds());
	}
}

void Player::SetAlive(bool dead)
{
	playerAlive = dead;
}

bool Player::IsAlvie() const
{
	return playerAlive;
}

void Player::MoveUp()
{
	if (sprite.getPosition().y > limitSize.y) {
		updown = -speed;
	}
	else {
		updown = 0;
	}
}

void Player::MoveDown()
{
	if (sprite.getPosition().y < Reg::sizeHeight - limitSize.y - spriteSize.y) {
		updown = speed;
	}
	else {
		updown = 0;
	}
}

void Player::MoveLeft()
{

	if (sprite.getPosition().x > limitSize.x) {
		leftright = -speed;
	}
	else {
		leftright = 0;
	}
}

void Player::MoveRight()
{
	if (sprite.getPosition().x < Reg::sizeWidth - limitSize.x - spriteSize.x) {
		leftright = speed;
	}
	else {
		leftright = 0;
	}
}

void Player::StopLeftRight()
{
	leftright = 0;
}

void Player::StopUpDown()
{
	updown = 0;
}

Player::Player(std::string s) : Objects(s), speed(200), playerAlive(false), moving(false)
{
	SetPosition(0, 0);
	SetType("Player");
}

Player::Player(std::string s, float _speed) : Objects(s), speed(_speed), playerAlive(false), moving(false)
{
	SetPosition(0, 0);
	SetType("Player");
}

Player::Player(std::string s, float _speed, int spriteSizeX, int spriteSizeY) : Objects(s), speed(_speed), playerAlive(false), moving(false)
{
	SetPosition(0, 0);
	spriteSize.x = spriteSizeX;
	spriteSize.y = spriteSizeY;
	SetType("Player");
}

Player::~Player()
{
}
