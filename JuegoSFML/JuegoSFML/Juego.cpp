#include "Juego.h"
#include "Scene.h"
#include "SFirstLevel.h"
#include "Reg.h"

void Juego::Run()
{
	window = new sf::RenderWindow(sf::VideoMode(Reg::sizeWidth, Reg::sizeHeight), "Juego SFML!");
	sf::Clock deltaClock;
	Scene* s = new Scene(window);
	s->ChangeScene(new SFirstLevel(window, s));
	
	while (window->isOpen())
	{
		sf::Time dt = deltaClock.restart();
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}
		window->clear();
		//Draw
		s->Update(dt);
		window->display();
	}
	delete window;
}

Juego::Juego()
{
}

Juego::~Juego()
{
}
