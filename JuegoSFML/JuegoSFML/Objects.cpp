#include "Objects.h"

Objects::Objects(std::string s) : type("error")
{
	if (!texture.loadFromFile(s))
	{
		std::cout << "Error al cargar la imagen " << s << std::endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

Objects::Objects(std::string s, int locationHeigh, int locationWidth, int sizeHeight, int sizeWidht) : type("error")
{
	if (!texture.loadFromFile(s, sf::IntRect(locationHeigh, locationWidth, sizeHeight, sizeWidht)))
	{
		std::cout << "Error al cargar la imagen " << s << std::endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

sf::Texture Objects::getTexture() const
{
	return texture;
}

sf::Sprite Objects::getSprite() const
{
	return sprite;
}

sf::Vector2f Objects::GetPosition()
{
	return sprite.getPosition();
}

void Objects::ChangeTexture(std::string s)
{
	if (!texture.loadFromFile(s))
	{
		std::cout << "Error al cargar la imagen " << s << std::endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

void Objects::SetPosition(float x, float y)
{
	sprite.setPosition(sf::Vector2f(x, y));
}

void Objects::SetLimits(int heightLimit, int widthLimit)
{
	limitSize.x = heightLimit;
	limitSize.y = widthLimit;
}


Objects::~Objects()
{
}

void Objects::SetType(std::string s)
{
	type = s;
}

std::string Objects::GetType()
{
	return type;
}
