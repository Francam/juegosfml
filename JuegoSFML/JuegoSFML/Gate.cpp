#include "Gate.h"

bool Gate::GetGateState() const
{
	return gateState;
}

void Gate::SetGateState(bool state)
{
	if (state == true) {
		gateState = true;
		ChangeTexture(openDir);
	}
	else {
		gateState = false;
		ChangeTexture(closedDir);
	}
}

void Gate::SwitchGateState()
{
	if (gateState == true) {
		gateState = false;
		ChangeTexture(closedDir);
	}
	else {
		gateState = true;
		ChangeTexture(openDir);
	}
}

void Gate::Update(sf::Time dt)
{
}

Gate::Gate(std::string openDirectory, std::string closeDirectory) : Objects(closeDirectory), gateState(false)
{
	openDir = openDirectory;
	closedDir = closeDirectory;
	SetType("Gate");
}

Gate::Gate(std::string openDirectory, std::string closeDirectory, float posX, float posY) : Objects(closeDirectory), gateState(false)
{
	SetPosition(posX, posY);
	openDir = openDirectory;
	closedDir = closeDirectory;	
	SetType("Gate");
}

Gate::~Gate()
{
}