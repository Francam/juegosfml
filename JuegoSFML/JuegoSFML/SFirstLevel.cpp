#include "SFirstLevel.h"

SFirstLevel::SFirstLevel(sf::RenderWindow* window, Scene* _parentScene) : Scene(window, _parentScene), parentScene(_parentScene), timer(0)
{
	if (!fuenteTexto.loadFromFile(DIRECTORY"ComicSansMS.ttf"))
	{
		std::cout << "Error al leer la funte." << std::endl;
	}

	py = new Player(DIRECTORY"Mage.png", 250, 50, 50);
	py->SetPosition(70, 70);
	py->SetLimits(50, 50);
	py->SetAlive(true);

	bk = new Background(DIRECTORY"Background.png");
	bk->SetPosition(0, 0);

	gate = new Gate(DIRECTORY"puerta-abierta.png", DIRECTORY"puerta-cerrada.png");
	gate->SetPosition(450, 300);

	mainEnemy = new EnemigoFollow(DIRECTORY"MainEnemy.png", 65, py);
	mainEnemy->SetPosition(700, 500);

	text_TimeText = new sf::Text("Tiempo: 0", fuenteTexto, 24);
	text_TimeText->setFillColor(sf::Color::Yellow);
	text_TimeText->setStyle(sf::Text::Bold);
	text_TimeText->setPosition(0, 0);

	gameOver = false;
}

SFirstLevel::~SFirstLevel()
{
	delete py;
	delete bk;
	delete gate;
	delete mainEnemy;
	delete text_TimeText;
}

void SFirstLevel::Update(sf::Time dt)
{
	timer += dt.asSeconds();
	if (timer > Reg::timeToWin && py->IsAlvie() && endLevel == false) {
		endLevel = true;
	}
	if(endLevel == true && gate->GetGateState() == false) {
		gate->SwitchGateState();
	}
	if (py->IsAlvie() && gameOver == false) {
		Collisiones(py, mainEnemy);
		Collisiones(py, gate);
	}
	//std::cout << timer << std::endl;
	//test only
	if (gameOver == true)
		mainEnemy->MoveStop();
	//
	MovementPlayer(py);
	mainEnemy->Update(dt);
	py->Update(dt);	
	window->draw(bk->getSprite());
	window->draw(gate->getSprite());
	window->draw(py->getSprite());	
	window->draw(mainEnemy->getSprite());
	int score = timer;
	text_TimeText->setString("Tiempo: " + std::to_string(score));
	window->draw(*text_TimeText);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		window->close();
	}
}

void SFirstLevel::MovementPlayer(Player * p)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		p->MoveLeft();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		p->MoveRight();
	}
	else {
		p->StopLeftRight();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		p->MoveUp();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		p->MoveDown();
	}
	else {
		p->StopUpDown();
	}
}

void SFirstLevel::Collisiones(Player * py, Objects * en)
{
	if (py->getSprite().getGlobalBounds().intersects(en->getSprite().getGlobalBounds()))
	{
		std::cout << "Collision " << en->GetType() << std::endl;
		if (en->GetType() == "Enemy")
		{
			py->SetAlive(false);
			gameOver = true;
		}
		if (en->GetType() == "Gate" && endLevel == true)
		{
			std::cout << "win" << std::endl;
			gameOver = true;
		}
	}
}