#pragma once
#include<SFML\Graphics.hpp>

class Scene
{
protected:
	sf::RenderWindow* window;
	Scene* s;
public:
	Scene(sf::RenderWindow* window);
	Scene(sf::RenderWindow* window, Scene* _s);
	virtual ~Scene();
	virtual void Update(sf::Time elapsed);
	void ChangeScene(Scene* _s);
};