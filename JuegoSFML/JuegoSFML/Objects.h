#pragma once
#include<iostream>
#include <SFML/Graphics.hpp>
class Objects
{
protected:
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Vector2f spriteSize;
	sf::Vector2f limitSize;
	std::string type;
public:
	Objects(std::string s);
	Objects(std::string s, int locationHeigh, int locationWidth, int sizeHeight, int sizeWidht);	
	sf::Texture getTexture() const;
	sf::Sprite getSprite() const;
	sf::Vector2f GetPosition();
	void ChangeTexture(std::string s);
	void virtual Update(sf::Time dt) = 0;
	void SetPosition(float x, float y);
	void SetLimits(int heightLimit, int widthLimit);
	virtual ~Objects();
	void SetType(std::string s);
	std::string GetType();
};