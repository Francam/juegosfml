#pragma once
#include "Objects.h"
class Background :
	public Objects
{
public:
	Background(std::string s);
	void Update(sf::Time dt);
	~Background();
};