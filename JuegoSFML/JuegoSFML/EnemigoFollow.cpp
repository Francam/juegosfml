#include "EnemigoFollow.h"

EnemigoFollow::EnemigoFollow(std::string s, float followspeed, Player* target) : Objects(s), maxSpeed(followspeed)
{
	speed = maxSpeed;
	py = target;
	SetType("Enemy");
}

void EnemigoFollow::Update(sf::Time dt)
{
	Move(dt);
}

sf::Vector2f EnemigoFollow::normalize(const sf::Vector2f & source)
{
	float length = sqrt((source.x * source.x) + (source.y * source.y));
	if (length != 0)
		return sf::Vector2f(source.x / length, source.y / length);
	else
		return source;
}

void EnemigoFollow::Move(sf::Time dt)
{
	sf::Vector2f diff = py->GetPosition() - GetPosition();
	sf::Vector2f dir = normalize(diff);
	sf::Vector2f offset = dir * speed * dt.asSeconds();
	sprite.move(offset);
	//std::cout << "Movimiento Follow " << offset.x << " " << offset.y << std::endl;
}

void EnemigoFollow::MoveStop()
{
	speed = 0;
}

EnemigoFollow::~EnemigoFollow()
{

}