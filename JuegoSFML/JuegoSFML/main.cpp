#include <iostream>
#include "Juego.h"
#include <Windows.h>

#if _DEBUG
#define USE_CONSOLE true
#else
#define USE_CONSOLE false
#endif

int main()
{
	if (!USE_CONSOLE)
	{
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	}
	Juego * g = new Juego();	
	g->Run();
	delete g;
	return 0;
}