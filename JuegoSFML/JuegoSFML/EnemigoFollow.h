#pragma once
#include "Objects.h"
#include "Player.h"
#include <math.h>
class EnemigoFollow :
	public Objects
{
private:
	Player* py;
	float speed;
	const float maxSpeed;
	int movingDirection;
	bool movingLRight;
	sf::Vector2i animSize;
public:
	EnemigoFollow(std::string s, float followspeed, Player* target);
	void Update(sf::Time dt);
	void Move(sf::Time dt);
	void MoveStop();
	sf::Vector2f normalize(const sf::Vector2f& source);
	~EnemigoFollow();
};