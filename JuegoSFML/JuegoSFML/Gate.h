#pragma once
#include "Objects.h"
class Gate :
	public Objects
{
private:
	bool gateState;
	std::string openDir;
	std::string closedDir;
public:
	bool GetGateState() const;
	void SetGateState(bool state);
	void SwitchGateState();
	void Update(sf::Time dt);
	Gate(std::string openDirectory, std::string closeDirectory);
	Gate(std::string openDirectory, std::string closeDirectory, float posX, float posY);
	~Gate();
};