#include "Scene.h"

Scene::Scene(sf::RenderWindow* window) : window(window)
{
}

Scene::Scene(sf::RenderWindow* window, Scene* _s) : window(window), s(_s)
{
}

Scene::~Scene()
{
	delete s;
}

void Scene::Update(sf::Time elapsed)
{
	s->Update(elapsed);
}

void Scene::ChangeScene(Scene * _s)
{
	Scene * aux = s;
	s = _s;
	delete aux;	
}