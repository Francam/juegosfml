#pragma once
#include "Objects.h"
class Player :
	public Objects
{
private:
	float speed; 
	bool moving;
	bool playerAlive;
	float updown;
	float leftright;
	sf::Vector2i spriteSize;
public:
	void Update(sf::Time dt);
	void SetAlive(bool dead);
	bool IsAlvie() const;
	void MoveUp();
	void MoveDown();
	void MoveLeft();	
	void MoveRight();
	void StopLeftRight();
	void StopUpDown();
	Player(std::string s);
	Player(std::string s, float _speed);
	Player(std::string s, float _speed, int spriteSizeX, int spriteSizeY);
	~Player();
};

